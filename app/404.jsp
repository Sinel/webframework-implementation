<%--
  Created by IntelliJ IDEA.
  User: sd
  Date: 18/11/2022
  Time: 16:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% String url = (String)request.getAttribute("url");%>
<html>
<head>
    <title>Contenue non trouvé</title>
</head>
<body>
  <h2>Le contenu de l'URL <%=url%> n'est pas disponible</h2>
</body>
</html>
