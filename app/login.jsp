<%--
  Created by IntelliJ IDEA.
  User: sd
  Date: 18/11/2022
  Time: 20:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="static mg.sinel.webframework.helpers.Url.baseUrl" %>
<html>
<head>
    <title>Connexion</title>
</head>
<body>
  <h2>Connexion Utilisateur</h2>
  <form action="<%=baseUrl(request,"User/login")%>" method="post">
    <div>
        <label for="username">Nom d'utilisateur :</label>
        <input type="text" name="username" id="username">
    </div>
    <div>
        <label for="password">Mot de passe :</label>
        <input type="password" name="password" id="password">
    </div>
    <button type="submit">Se connecter</button>
  </form>
</body>
</html>
