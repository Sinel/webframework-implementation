<%@ page import="mg.sd.company.company.Departement" %>
<%@ page import="static mg.sinel.webframework.helpers.Url.baseUrl" %>
<%@ page import="mg.sd.company.company.Hobby" %><%--
  Created by IntelliJ IDEA.
  User: sd
  Date: 09/11/2022
  Time: 13:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Departement[] departements = (Departement[]) request.getAttribute("departements");
    Hobby[] hobbies = (Hobby[]) request.getAttribute("hobbies");
%>
<html>
<head>
    <title>Saisie employee</title>
</head>
<body>
<h1>Saisie d'un employée</h1>
    <form action="<%=baseUrl(request,"Employee/enregistrement")%>" method="post">
        <div>
            <label>Nom :</label>
        </div>
        <div>
            <input type="text" name="nom">
        </div>
        <div>
            <label>Prenom :</label>
        </div>
        <div>
            <input type="text" name="prenom">
        </div>
        <div>
            <label>Date d'embauche :</label>
        </div>
        <div>
            <input type="date" name="dateEmbauche">
        </div>
        <div>
            <label>Departement :</label>
        </div>
        <div>
            <select name="departement">
                <% for(Departement departement : departements) {%>
                <option value="<%=departement.getId()%>"><%=departement.getName()%></option>
                <% } %>
            </select>
        </div>
        <div>
            <label>Hobbies :</label>
        </div>
        <% for(Hobby hobby : hobbies) { %>
        <div>
            <label><%=hobby.getNom() %></label>
            <input type="checkbox" name="hobbies" value="<%=hobby.getNom()%>">
        </div>
        <% } %>
        <button type="submit"> Enregistrer</button>
    </form>
</body>
</html>
