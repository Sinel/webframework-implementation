<%@ page import="java.time.LocalDateTime" %><%--
  Created by IntelliJ IDEA.
  User: sd
  Date: 07/11/2022
  Time: 13:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><%=request.getAttribute("title")%></title>
</head>
<body>
<h1><%=request.getAttribute("title")%></h1>
<h2><%=request.getAttribute("message")%></h2>
<p>Action effectue le <%=((LocalDateTime)request.getAttribute("date")).toString()%></p>
</body>
</html>
