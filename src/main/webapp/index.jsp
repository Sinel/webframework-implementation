<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="static mg.sinel.webframework.helpers.Url.baseUrl" %>
<!DOCTYPE html>
<html>
<head>
    <title>Test framework</title>
</head>
<body>
<h1>URLS de test</h1>
<h2>Parametres</h2>
<p><a href="<%=baseUrl(request,"Employee/saisie")%>">Saisir un employé</a></p>
<p><a href="<%=baseUrl(request,"Employee/show?id=1")%>">Afficher l'employee 1</a></p>
<h2>Authentification</h2>

<p><a href="<%=baseUrl(request,"User/info")%>">Information sur l'utilisateur connecté en utilisant l'HashMap</a> (login necessaire)</p>
<p><a href="<%=baseUrl(request,"User/infoAlt")%>">Information sur l'utilisateur connecte en utilisant l'Annotation</a> (login necessaire)</p>
<p><a href="<%=baseUrl(request,"User/login")%>">Login</a></p>
</body>
</html>