package mg.sd.company.company;

import mg.sinel.webframework.ModelAction;
import mg.sinel.webframework.ModelView;
import mg.sinel.webframework.anotations.Get;
import mg.sinel.webframework.anotations.Post;
import mg.sinel.webframework.anotations.ServletClass;
import mg.sinel.webframework.anotations.SessionAttribut;
import mg.sinel.webframework.helpers.Url;
import mg.sinel.webframework.model.SessionModel;

import java.util.HashMap;

@ServletClass
public class User extends SessionModel {
    private String username;
    private String password;

    @Get(url = "login")
    public ModelView loginForm() {
        return new ModelView(ModelAction.LOAD_PAGE,"login.jsp");
    }

    @Post(url = "login")
    public ModelView login() {
        ModelView data = new ModelView(ModelAction.REDIRECT,"User/info");
        HashMap<String, Object> attributes = getSession();
        attributes.put("user",this);
        return data;
    }

    @Get
    public ModelView info() {
        User user = (User)getSession().get("user");
        if(user == null) {
            return new ModelView(ModelAction.REDIRECT,"User/login");
        }
        user.username = user.username + ":map";
        return new ModelView(ModelAction.LOAD_PAGE,"user.jsp");
    }

    @Get
    public ModelView infoAlt(@SessionAttribut("user") User user) {
        if(user == null) {
            return new ModelView(ModelAction.REDIRECT,"User/login");
        }
        user.username = user.username + ":annotation";
        return new ModelView(ModelAction.LOAD_PAGE,"user.jsp");
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
