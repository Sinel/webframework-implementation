package mg.sd.company.company;

public class Hobby {
    String nom;

    public Hobby(String nom) {
        setNom(nom);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
