package mg.sd.company.company;

import mg.sinel.webframework.ModelAction;
import mg.sinel.webframework.ModelView;
import mg.sinel.webframework.anotations.Get;
import mg.sinel.webframework.anotations.Post;
import mg.sinel.webframework.anotations.ServletClass;
import mg.sinel.webframework.anotations.ServletMethod;
import mg.sinel.webframework.model.SessionModel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;

@ServletClass()
public class Employee {
    private Integer id;
    private String nom;
    private String prenom;
    private LocalDate dateEmbauche;
    private Integer departement;
    private String[] hobbies;

    public Employee() {}
    public Employee(Integer id, String nom, String prenom, LocalDate dateEmbauche, Integer departement, String[] hobbies) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateEmbauche = dateEmbauche;
        this.departement = departement;
        this.hobbies = hobbies;
    }

    @Get
    public ModelView saisie() {
        ModelView data = new ModelView(ModelAction.LOAD_PAGE,"saisie.jsp");
        HashMap<String, Object> attributes = data.getAttributes();
        Departement[] departements = new Departement[3];
        departements[0] = new Departement(0,"Informatique");
        departements[1] = new Departement(1,"Achat");
        departements[2] = new Departement(2,"Ressources humaines");
        Hobby[] hobbies = new Hobby[3];
        hobbies[0] = new Hobby("Foot");
        hobbies[1] = new Hobby("Basket");
        hobbies[2] = new Hobby("Jeux");
        attributes.put("hobbies",hobbies);
        attributes.put("departements",departements);
        return data;
    }

    @Get
    public ModelView show() {
        ModelView data = new ModelView(ModelAction.LOAD_PAGE,"success.jsp");
        HashMap<String, Object> attributes = data.getAttributes();
        Employee[] employees = new Employee[3];
        employees[0] = new Employee(0,"Doe","John",LocalDate.of(2020,1,1),0,new String[]{"Foot","Basket"});
        employees[1] = new Employee(1,"Doe","Jane",LocalDate.of(2020,1,1),1,new String[]{"Foot","Jeux"});
        employees[2] = new Employee(2,"Doe","Jack",LocalDate.of(2020,1,1),2,new String[]{"Basket","Jeux"});
        attributes.put("employee",employees[id]);
        return data;
    }

    @Post
    public ModelView enregistrement() {
        ModelView data = new ModelView(ModelAction.LOAD_PAGE,"success.jsp");
        HashMap<String, Object> attributes = data.getAttributes();
        attributes.put("employee",this);
        return data;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public LocalDate getDateEmbauche() {
        return dateEmbauche;
    }

    public void setDateEmbauche(LocalDate dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    public Integer getDepartement() {
        return departement;
    }

    public void setDepartement(Integer departement) {
        this.departement = departement;
    }

    public String[] getHobbies() {
        return hobbies;
    }

    public void setHobbies(String[] hobbies) {
        this.hobbies = hobbies;
    }
}
